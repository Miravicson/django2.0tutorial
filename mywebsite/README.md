# Creating a postgres database and a user

1. Login to the interactive Postgres session by typing

$ sudo -u postgres psql

2. First, create a database for your project:
$ CREATE DATABASE myproject;

3. Next, create a database user for our project. Make sure to select a secure password:

$ CREATE USER myprojectuser WITH PASSWORD 'password';

4. Modify the connection parameters of myprojectuser to requirments stipulated by django. These requirments include
    a. Setting the default encoding to UTF-8,
    b. Setting the default transaction isolation scheme to "read committed".
    c. Setting the timezone.

$ ALTER ROLE myprojectuser SET client_encoding TO 'utf8';
$ ALTER ROLE myprojectuser SET default_transaction_isolation TO 'read committed';
$ ALTER ROLE myprojectuser SET timezone TO 'UTC';
5. Finally, you have to give the new user access to administer our new user to administer our new database:

$ GRANT ALL PRIVILEGES ON DATABASE myproject TO myprojectuser;

6. Use the following command to exit the PostgreSQL prompt
$ \q

# Database configuration structure for django settings.py file

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'myproject',
        'USER': 'myprojectuser',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '',
    }
}
