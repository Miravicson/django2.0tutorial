from django.urls import path

from . import views

app_name = 'mypolls'

urlpatterns = [
    # path('', views.index, name='index'),
    # modification to use generic views
    path('', views.IndexView.as_view(), name='index'),
    # ex: /mypolls/5/
    # path('<int:question_id>/', views.detail, name='detail'),
    # modification to use generic views
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    # ex: /mypolls/5/results/
    # path('<int:question_id>/results/', views.results, name='results'),
    # modification to use generic view
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    # ex: /polls/5/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
